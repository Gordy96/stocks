<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::group(['prefix'=>'login'], function () {
    Route::get('/', 'Auth\\LoginController@showLoginForm')->name('login');
    Route::post('/', 'Auth\\LoginController@login');
});
Route::get('/logout', 'Auth\\LoginController@logout');
Route::group(['prefix'=>'register'], function () {
    Route::get('/', 'Auth\\RegisterController@showRegistrationForm')->name('register');
    Route::post('/', 'Auth\\RegisterController@register');
});
Route::group(['middleware'=>['auth']], function(){
    Route::group(['prefix'=>'admin', 'middleware'=>['role:admin']], function(){
        Route::get('/', 'Admin\\CabinetController@index');
        Route::group(['prefix'=>'stocks'], function(){
            Route::get('/', 'Admin\\CabinetController@stocks');
            Route::group(['prefix'=>'{id}'], function () {
                Route::get('/', 'Admin\\CabinetController@stocks');
            });
        });
        Route::group(['prefix'=>'accounts'], function(){
            Route::get('/', 'Admin\\CabinetController@accounts');
            Route::group(['prefix'=>'{tid}'], function () {
                Route::get('/', 'Admin\\CabinetController@accounts');
            });
        });
        Route::group(['prefix'=>'transactions'], function(){
            Route::get('/', 'Admin\\CabinetController@transactions');
            Route::group(['prefix'=>'{tid}'], function () {
                Route::get('/', 'Admin\\CabinetController@transactions');
            });
        });
        Route::group(['prefix'=>'users'], function(){
            Route::get('/', 'Admin\\CabinetController@users');
            Route::group(['prefix'=>'{uid}'], function () {
                Route::get('/', 'Admin\\CabinetController@users');
            });
        });
    });
    Route::group(['prefix'=>'cabinet'], function(){
        Route::get('/', 'CabinetController@Index');
        Route::group(['prefix'=>'accounts'], function(){
            Route::get('/', 'CabinetController@Accounts');
            Route::group(['prefix'=>'{id}'], function(){
                Route::get('/', 'CabinetController@Account');
                Route::post('/', 'CabinetController@EditAccount');
                Route::any('/topup', 'CabinetController@TopUp');
                Route::any('/delete', 'CabinetController@DeleteAccount');
            });
            Route::post('/', 'CabinetController@NewAccount');
        });
        Route::group(['prefix'=>'balances'], function(){
            Route::get('/{id}', 'CabinetController@Balance');
        });
        Route::group(['prefix'=>'stocks'], function(){
            Route::get('/{id}', 'CabinetController@Stock');
        });

        Route::group(['prefix'=>'settings'], function(){
            Route::get('/', 'CabinetController@Settings');
            Route::post('/', 'CabinetController@UpdateSettings');
        });
        Route::group(['prefix'=>'password'], function(){
            Route::post('/reset', 'CabinetController@ResetPassword');
        });
    });
    Route::get('/', function () {
        if(\Illuminate\Support\Facades\Auth::user()->hasRole('admin'))
            return redirect('/admin');
        return redirect('/cabinet');
    });
});
