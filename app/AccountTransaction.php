<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AccountTransaction extends Model
{
    public $fillable = [
        'value', 'debit', 'account_id', 'balance_id', 'info', 'created_at', 'date'
    ];

    public $hidden = [
        'account_id', 'balance_id'
    ];

    public $dates = [
        'created_at',
        'updated_at'
    ];

    public $casts = [
        'info'=>'object'
    ];

    public function getInfoAttribute(){
        return $this->castAttribute('info', $this->attributes['info']);
    }

    public function account(){
        return $this->belongsTo(Account::class);
    }

    public function balance(){
        return $this->belongsTo(AccountBalance::class);
    }

    public function confirm(){
        $v = $this->value * ($this->debit ? -1 : 1);
        $this->balance->value += $v;
        $this->balance->save();
    }

    public function revert(){
        $v = $this->value * ($this->debit ? 1 : -1);
        $this->balance->value += $v;
        $this->balance->save();
    }
}
