<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Setting extends Model
{
    public $fillable = [
        'name', 'type'
    ];

    public function users(){
        return $this->belongsToMany(
            Setting::class,
            'user_setting',
            'setting_id',
            'user_id')
            ->using(UserSetting::class);
    }
}
