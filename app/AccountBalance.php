<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AccountBalance extends Model
{
    public $fillable = [
        'value',
        'account_id',
        'currency_id'
    ];

    protected $hidden = [
        'account_id'
    ];

    public $dates = [
        'created_at',
        'updated_at'
    ];

    public function account(){
        return $this->belongsTo(Account::class);
    }

    public function currency(){
        return $this->belongsTo(Currency::class);
    }

    public function transactions(){
        return $this->hasMany(AccountTransaction::class, 'balance_id');
    }
}
