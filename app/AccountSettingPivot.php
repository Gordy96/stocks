<?php

namespace App;

use Illuminate\Database\Eloquent\Relations\Pivot;

class AccountSettingPivot extends Pivot
{
    public $fillable = [
        'account_id',
        'setting_id',
        'value',
        'parameters'
    ];

    public $casts = [
        'parameters'=>'array'
    ];

    public $hidden = [
        'account_id',
        'setting_id'
    ];

    public $dates = [
        'created_at',
        'updated_at'
    ];

    public function setting(){
        return $this->belongsTo(AccountSetting::class, 'setting_id');
    }

    public function account(){
        return $this->belongsTo(Account::class, 'account_id');
    }

    protected function getCastType($key)
    {
        if ($key == 'value' && !empty($this->setting->type)) {
            return $this->setting->type;
        } else {
            return parent::getCastType($key);
        }
    }

    public function getValueAttribute(){
        return $this->castAttribute('value', $this->attributes['value']);
    }
}
