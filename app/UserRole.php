<?php

namespace App;

use Illuminate\Database\Eloquent\Relations\Pivot;

class UserRole extends Pivot
{
    public $fillable = [
        'user_id', 'role_id', 'granter_id', 'expires_at'
    ];

    public $dates = [
        'expires_at',
        'created_at',
        'updated_at'
    ];

    public $hidden = [
        'expires_at',
        'user_id',
        'role_id'
    ];

    public function granter(){
        return $this->belongsTo(User::class, 'granter_id', 'id');
    }
}
