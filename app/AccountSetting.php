<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AccountSetting extends Model
{
    public $fillable = [
        'name', 'type'
    ];

    public function accounts(){
        return $this->belongsToMany(
            Account::class,
            'account_account_setting',
            'setting_id',
            'account_id')
            ->using(AccountSettingPivot::class);
    }
}
