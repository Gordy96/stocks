<?php

namespace App;

use Illuminate\Database\Eloquent\Relations\Pivot;

class UserSetting extends Pivot
{
    public $fillable = [
        'user_id', 'setting_id', 'value', 'parameters'
    ];

    public $casts = [
        'parameters'=>'array'
    ];

    public $hidden = [
        'user_id',
        'setting_id'
    ];

    public $dates = [
        'created_at',
        'updated_at'
    ];
}
