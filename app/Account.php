<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Account extends Model
{
    public $fillable = [
        'stock_id',
        'user_id',
        'status_id',
        'total',
        'description'
    ];

    protected $hidden = [
        'stock_id',
        'user_id'
    ];

    public $dates = [
        'created_at',
        'updated_at'
    ];

    public function stock() {
        return $this->belongsTo(Stock::class);
    }

    public function user() {
        return $this->belongsTo(User::class);
    }

    public function balances(){
        return $this->hasMany(AccountBalance::class, 'account_id');
    }

    public function transactions(){
        return $this->hasMany(AccountTransaction::class, 'account_id');
    }

    public function status(){
        return $this->belongsTo(AccountStatus::class, 'status_id');
    }

    public function settings(){
        return $this->belongsToMany(
            AccountSetting::class,
            'account_account_setting',
            'account_id',
            'setting_id',
            'id',
            'id'
        )->withPivot(['value', 'parameters','created_at', 'updated_at'])->using(AccountSettingPivot::class)->as('info');
    }
}
