<?php

namespace App\Http\Controllers;

use App\Account;
use App\AccountSetting;
use App\Jobs\ParseAccountInfo;
use App\Jobs\UpdateAccountTotal;
use App\Stock;
use Illuminate\Auth\Events\PasswordReset;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\ValidationException;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

class CabinetController extends Controller
{
    use ValidatesRequests;

    public function __construct()
    {
        Validator::extend('password', function ($attribute, $value, $parameters, $validator) {
            return Hash::check($value, Auth::user()->getAuthPassword());
        });
    }

    public function Index(Request $request)
    {
        return view('cabinet.index');
    }

    public function Accounts(Request $request)
    {
        $accounts = Auth::user()->accounts()->with(['stock','balances', 'balances.currency'])->paginate();
        return view('cabinet.accounts.index', ['accounts'=>$accounts]);
    }

    public function Account(Request $request, $id)
    {
        $account = Auth::user()->accounts()->with([
            'stock',
            'balances',
            'balances.currency'
        ])->find($id);
        return view('cabinet.accounts.single', ['account'=>$account]);
    }

    public function Stock(Request $request, $id)
    {
        $stock = Stock::with([
            'currencies'
        ])->find($id);
        return view('cabinet.stocks.single', ['stock'=>$stock]);
    }

    public function NewAccount(Request $request)
    {
        $rules = [
            'stock'=>'required|exists:stocks,id',
            'username'=>'sometimes|required|min:6',
            'password'=>'required_with:username|min:8',
            'key'=>'sometimes|required|min:8',
            'secret'=>'required_with:key|min:8',
            'proxy'=>'sometimes|nullable'
        ];
        $filtered = $this->validate($request, $rules);
        if(!$filtered){
            throw new BadRequestHttpException();
        }

        $stock = Stock::findOrFail($filtered['stock']);
        $acc = Auth::user()->accounts()->save(new Account(['stock_id'=>$stock->id]));
        $creds_setting = AccountSetting::where('name', 'credentials')->firstOrFail();
        if (isset($filtered['key'])){
            $creds = [
                'key'=>$filtered['key'],
                'secret'=>$filtered['secret']
            ];
            if(isset($filtered['username'])){
                $creds['username'] = $filtered['username'];
                $creds['password'] = $filtered['password'];
            }
            $acc->settings()->attach($creds_setting, [
                'value'=>json_encode($creds)
            ]);
        } elseif(isset($filtered['username'])){
            $acc->settings()->attach($creds_setting->id, [
                'value'=>json_encode([
                    'username'=>$filtered['username'],
                    'password'=>$filtered['password']
                ])
            ]);
        }
        if(isset($filtered['proxy'])){
            $proxy_setting = AccountSetting::where('name', 'proxy')->firstOrFail();
            $acc->settings()->attach($proxy_setting->id, [
                'value'=>$filtered['proxy']
            ]);
        }
        ParseAccountInfo::createJob($acc->id);
        UpdateAccountTotal::createJob($acc->id);
        return redirect()->back();
    }

    public function Settings(Request $request){
        return view('cabinet.settings');
    }

    public function ResetPassword(Request $request){
        $rules = [
            'password_old'=>'required|password',
            'password' => 'required|confirmed|min:6',
        ];
        try{
            $this->validate($request, $rules);
        } catch (ValidationException $e){
            return redirect()->back()->withErrors($e->validator->getMessageBag());
        }
        $user = Auth::user();
        $password = $request->get('password');
        $user->password = Hash::make($password);
        $user->save();

        event(new PasswordReset($user));
        return redirect()->back();
    }

    public function TopUp(Request $request, $id)
    {
        $account = Auth::user()->accounts()->findOrFail($id);
        $api = $account->stock->createExecutorInstance($account);
        return $api->topUp();//view("layouts.autoform",['form'=>$api->topUp($request->get('amount'))]);
    }

    public function DeleteAccount(Request $request, $id){
        $account = Auth::user()->accounts()->findOrFail($id);
        $account->delete();
        return redirect()->back();
    }

    public function Balance(Request $request, $id){
        $balance = Auth::user()->balances()->findOrFail($id);
        return view('cabinet.transactions.all', ['balance'=>$balance]);
    }
}
