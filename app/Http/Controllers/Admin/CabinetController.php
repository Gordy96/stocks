<?php

namespace App\Http\Controllers\Admin;

use App\Account;
use App\AccountTransaction;
use App\Stock;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CabinetController extends Controller
{
    public function index(Request $request){
        return redirect('/admin/users');
    }
    public function stocks(Request $request, $id = null){
        if(!$id){
            $stocks = Stock::paginate(20);
            return view('admin.stocks.all', ['stocks'=>$stocks]);
        } else {
            $stock = Stock::findOrFail($id);
            return view('admin.stocks.single', ["stock"=>$stock]);
        }
    }
    public function users(Request $request, $id = null){
        if(!$id){
            $users = User::paginate(20);
            return view('admin.users.all', ['users'=>$users]);
        } else {
            $user = User::findOrFail($id);
            return view('admin.users.single', ["user"=>$user]);
        }
    }

    public function accounts(Request $request, $id = null){
        if(!$id){
            $accounts = Account::paginate();
            return view('admin.accounts.all', ['accounts'=>$accounts]);
        } else {
            $account = Account::findOrFail($id);
            return view('admin.accounts.single', ["account"=>$account]);
        }
    }
}
