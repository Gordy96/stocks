<?php

namespace App\Jobs;

use App\Account;
use App\AccountSetting;
use App\AccountTransaction;
use App\Currency;
use App\Helpers\Exceptions\BadAuthException;
use App\Helpers\StockApiInterface;
use App\Stock;
use GuzzleHttp\Cookie\SetCookie;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class ParseAccountInfo implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    const DELAY = 300;
    const QUEUE = 'account.update.balances.';

    private $account_id;
    /**
     * @var StockApiInterface
     */
    private $api;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($aid)
    {
        $this->account_id = $aid;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $account = Account::with(['stock', 'balances', 'balances.currency', 'settings'])->find($this->account_id);
        if(!$account)
            return;


        $this->api = $account->stock->createExecutorInstance($account);
        $balances = $this->api->getBalances();
        foreach ($balances as $balance){
            $currency = Currency::where('name',  $balance['name'])->firstOrCreate(['name'=>$balance['name']]);
            $account->stock->currencies()->sync([$currency->id], false);

            $b = $account->balances()->whereHas('currency', function($q) use($balance){
                $q->where('name', $balance['name']);
            })->firstOrNew([
                'account_id'=>$account->id,
                'currency_id'=>$currency->id
            ]);
            $b->value = $balance['balance'];
            $b->save();
        }
        $tr = $this->api->getTransactionHistory();
        if($tr)
            $account->transactions()->saveMany($tr);
        $this->api->saveState();
        $account->touch();
        static::createJob($this->account_id);
    }

    public static function createJob($sid){
        dispatch(new static($sid))->onQueue(static::getQueueName($sid))->delay(static::DELAY);
    }

    public static function getQueueName($aid){
        return static::QUEUE . $aid;
    }
}
