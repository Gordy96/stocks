<?php

namespace App\Jobs;

use App\Account;
use App\Stock;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\DB;

class QueueKeeper implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    const QUEUE = 'queue.keeper';
    const DELAY = 900;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $accounts = Account::pluck('id');
        $jobs = DB::table('jobs')
            ->where('queue', 'like', ParseAccountInfo::QUEUE . "%")
            ->select(['id','queue'])
            ->get()
            ->pluck('id', 'queue');
        foreach ($accounts as $account){
            if(!isset($jobs[ParseAccountInfo::QUEUE.$account]))
                ParseAccountInfo::createJob($account);
        }
        $jobs = DB::table('jobs')
            ->where('queue', 'like', UpdateAccountTotal::QUEUE . "%")
            ->select(['id','queue'])
            ->get()
            ->pluck('id', 'queue');
        foreach ($accounts as $account){
            if(!isset($jobs[UpdateAccountTotal::QUEUE.$account]))
                UpdateAccountTotal::createJob($account);
        }
        $stocks = Stock::pluck('id');
        $jobs = DB::table('jobs')
            ->where('queue', 'like', UpdateStockRates::QUEUE . "%")
            ->select(['id','queue'])
            ->get()
            ->pluck('id', 'queue');
        foreach ($stocks as $stock){
            if(!isset($jobs[UpdateStockRates::QUEUE.$stock]))
                UpdateStockRates::createJob($stock);
        }
        static::createJob();
    }

    public static function createJob(){
        dispatch(new static())->onQueue(static::getQueueName())->delay(static::DELAY);
    }

    public static function getQueueName(){
        return static::QUEUE;
    }
}
