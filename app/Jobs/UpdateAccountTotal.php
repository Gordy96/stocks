<?php

namespace App\Jobs;

use App\Account;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Log;

class UpdateAccountTotal implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    const DELAY = 300;
    const QUEUE = 'account.update.total.';

    private $aid;
    /**
     * Create a new job instance.
     * @param int $aid
     * @return void
     */
    public function __construct($aid)
    {
        $this->aid = $aid;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Collection|\Illuminate\Database\Eloquent\Model|Account
     */
    private function retrieveModel(){
        return Account::with(['balances', 'balances.currency', 'stock'])->find($this->aid);
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $account = $this->retrieveModel();
        if(!$account)
            return;
        $skip = true;
        foreach ($account->balances as $balance){
            $skip &= $balance->value == 0;
        }
        if(!$skip){
            Log::info("account #".$account->id);
            $usd_curr_ids = [];
            try{
                foreach($account->stock->currencies()->wherePivot('virtual', false)->get() as $currency)
                    array_push($usd_curr_ids, $currency->id);
                $btc_to_usd = $account->stock->currencies()->where('name', 'btc')->wherePivot('virtual', false)->firstOrFail()->pivot->rate;
                $rates = [];
                foreach($account->stock->currencies as $currency)
                    $rates[$currency->id] = $currency->pivot->rate;
                $usd_total = 0;
                $btc_total = 0;
                foreach ($account->balances as $balance){
                    if(in_array($balance->currency_id, $usd_curr_ids)){
                        $usd_total += ($balance->value * ($balance->currency->name === 'USD' ? 1 : $rates[$balance->currency_id]));
                    } else {
                        $btc_total += $balance->value * $rates[$balance->currency_id];
                    }
                }
                $total = $usd_total + ($btc_total * $btc_to_usd);
                $account->total = $total;
                $account->save();
            } catch (\Exception $exception) {
                Log::error($exception);
            }
        }
        static::createJob($this->aid);
    }

    public static function createJob($sid){
        dispatch(new static($sid))->onQueue(static::getQueueName($sid))->delay(static::DELAY);
    }

    public static function getQueueName($aid){
        return static::QUEUE . $aid;
    }
}
