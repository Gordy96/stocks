<?php

namespace App\Jobs;

use App\Currency;
use App\Stock;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class UpdateStockRates implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    const DELAY = 300;
    const QUEUE = 'stock.update.rates.';

    private $stock_id;

    /**
     * Create a new job instance.
     * @param int $stock
     * @return void
     */
    public function __construct($stock)
    {
        $this->stock_id = $stock;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Collection|\Illuminate\Database\Eloquent\Model|Stock
     */

    private function retrieveStock()
    {
        return Stock::with(['currencies'])->find($this->stock_id);
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $stock = $this->retrieveStock();
        if(!$stock)
            return;
        $api = $stock->createExecutorInstance(null);
        $rates = $api->getRates();
        foreach($rates as $name => $rate){
            $currency = Currency::where('name',  $name)->firstOrCreate(['name'=>$name]);
            $data = [
                'rate'=>$rate['rate'],
                'virtual'=>$rate['to'] === 'BTC'
            ];
            if(($persist = $stock->currencies()->where('name', $name)->first()) && $persist->pivot->rate > 0){
                $data['change_rate'] = (($persist->pivot->rate - floatval($rate['rate'])) / $persist->pivot->rate) * 100;
            }
            $stock->currencies()->sync([$currency->id=>$data], false);
        }
        $stock->touch();
        static::createJob($this->stock_id);
    }
    public static function getQueueName($sid){
        return static::QUEUE . $sid;
    }

    public static function createJob($sid){
        dispatch(new static($sid))->onQueue(static::getQueueName($sid))->delay(static::DELAY);
    }
}
