<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    protected $fillable = [
        'name',
    ];

    public function users(){
        return $this->belongsToMany(
            User::class,
            'user_role',
            'role_id',
            'user_id')
            ->using(UserRole::class);
    }
}
