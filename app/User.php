<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Events\TransactionBeginning;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\Log;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public $dates = [
        'created_at',
        'updated_at'
    ];

    public function roles(){
        return $this->belongsToMany(
            Role::class,
            'user_role',
            'user_id',
            'role_id')
            ->using(UserRole::class);
    }

    public function settings(){
        return $this->belongsToMany(
            Setting::class,
            'user_setting',
            'user_id',
            'setting_id')
            ->using(UserSetting::class);
    }

    public function accounts($stock = null){
        $q = $this->hasMany(Account::class, 'user_id', 'id');
        if($stock instanceof Stock){
            $q = $q->where('stock_id', $stock->id);
        }
        return $q;
    }

    public function stocks(){
        return $this->hasManyThrough(
            Stock::class,
            Account::class,
            'user_id',
            'id',
            'id',
            'stock_id'
        );
    }

    public function transactions(){
        return $this->hasManyThrough(
            AccountTransaction::class,
            Account::class,
            'user_id',
            'account_id',
            'id',
            'id'
        );
    }

    public function balances(){
        return $this->hasManyThrough(
            AccountBalance::class,
            Account::class,
            'user_id',
            'account_id',
            'id',
            'id'
        );
    }

    public function hasRole($name){
        if(starts_with($name, '!'))
            $q = $this->roles()->where('name', substr($name, 1));
        else
            $q = $this->roles()->where('name', $name);
        if(starts_with($name, '!'))
            return $q->count() == 0;
        else
            return $q->count() !== 0;
    }
}
