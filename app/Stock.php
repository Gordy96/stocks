<?php

namespace App;

use App\Helpers\StockApiInterface;
use Illuminate\Database\Eloquent\Model;

class Stock extends Model
{
    public $fillable = [
        'name', 'executor'
    ];

    public $dates = [
        'created_at',
        'updated_at'
    ];

    public function users() {
        return $this->hasManyThrough(
            User::class,
            Account::class,
            'stock_id',
            'id',
            'id',
            'user_id'
        );
    }

    public function accounts(){
        return $this->hasMany(Account::class);
    }

    public function balances(){
        return $this->hasManyThrough(
            AccountBalance::class,
            Account::class,
            'stock_id',
            'account_id',
            'id',
            'id'
        );
    }

    public function transactions(){
        return $this->hasManyThrough(
            AccountTransaction::class,
            Account::class,
            'stock_id',
            'account_id',
            'id',
            'id'
        );
    }

    public function currencies(){
        return $this->belongsToMany(Currency::class, 'stock_currency')->withPivot([
            'virtual',
            'rate',
            'change_rate'
        ])->orderBy('name');
    }


    /**
     * @param array ...$credentials
     * @return StockApiInterface
     */
    public function createExecutorInstance(...$credentials){
        return new $this->executor(...$credentials);
    }
}
