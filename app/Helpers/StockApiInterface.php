<?php
/**
 * Created by PhpStorm.
 * User: Johnny
 * Date: 01/06/2018
 * Time: 11:59
 */

namespace App\Helpers;


use App\Account;
use Carbon\Carbon;
use GuzzleHttp\Cookie\CookieJar;

interface StockApiInterface
{
    /**
     * StockApiInterface constructor.
     * @param Account|string $account
     */
    public function __construct($account);

    /**
     * @return array
     */
    public function getBalances();

    /**
     * @return array
     */
    public function getTransactionHistory();

    public function getRates();

    public function saveState();

    /**
     * @param float $amount
     * @param string $currency
     * @param string $method
     * @return mixed
     */
    public function topUp();

    public function Auth();
}