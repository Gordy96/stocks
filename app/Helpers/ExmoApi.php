<?php
/**
 * Created by PhpStorm.
 * User: Johnny
 * Date: 09/06/2018
 * Time: 20:05
 */

namespace App\Helpers;


use App\Account;
use App\AccountSetting;
use App\AccountStatus;
use App\AccountTransaction;
use App\Helpers\Captcha\ApiErrorException;
use App\Helpers\Captcha\NoCaptcha;
use App\Helpers\Exceptions\BadAuthException;
use Carbon\Carbon;
use GuzzleHttp\Client;
use GuzzleHttp\Cookie\CookieJar;
use GuzzleHttp\Cookie\SetCookie;
use GuzzleHttp\Psr7\Uri;
use GuzzleHttp\TransferStats;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Support\Facades\Log;

class ExmoApi implements StockApiInterface
{

    use ValidatesRequests;

    /**
     * @var Client
     */
    private $client = null;

    /**
     * @var Account|null
     */

    private $account = null;

    const base = 'https://api.exmo.com/v1/';

    /**
     * @var string
     */
    private $key = '';

    /**
     * @var string
     */
    private $secret = '';

    /**
     * @var string
     */
    private $email = null;

    /**
     * @var string
     */
    private $password = null;

    /**
     * @var string
     */
    private $ua = '';

    /**
     * @var object
     */
    private $state = null;

    private $currencies = [
        'RUR'=>'RUB'
    ];

    private $reverse_currencies;

    private $proxy = '';

    public function __construct($account)
    {
        $defs = [
            'base_uri'=>static::base,
            'verify'=>false,
            'cookies'=>true,
            'headers'=>[
            ]
        ];

        $this->reverse_currencies = array_flip($this->currencies);

        if($account instanceof Account){
            $this->account = $account;

            $ua = $account->settings()->where('name', 'user-agent')->first();
            if(!$ua){
                $ua = RandomUserAgent::getRandomUserAgent();
                $setting = AccountSetting::where('name', 'user-agent')->first();
                $account->settings()->attach($setting->id, [
                    'value'=>$ua
                ]);
            } else {
                $ua = $ua->info->value;
            }

            $defs['headers']['User-Agent'] = $ua;
            $this->ua = $ua;

            $creds = $account->settings()->where('name', 'credentials')->first()->info->value;
            $this->key = $creds->key;
            $this->secret = $creds->secret;
            $this->email = $creds->username;
            $this->password = $creds->password;
            $proxy = $account->settings()->where('name', 'proxy')->first();

            if($proxy){
                $defs['proxy'] = $proxy->info->value;
                $this->proxy = $proxy->info->value;
            }

            if(($cookies = $account->settings()->where('name', 'cookies')->first()) && $cookies->info->value) {
                $defs['cookies'] = $this->setCookies($cookies->info->value);
            }
            $this->getAccountState();
        } elseif(is_string($account)){
            $defs['proxy'] = $account;
        }
        $this->client = new Client($defs);
    }

    private function setCookies($cookies)
    {
        $c = new CookieJar();
        foreach ($cookies as $cookie)
            $c->setCookie(
                new SetCookie([
                    'Name'     => $cookie->name,
                    'Value'    => $cookie->value,
                    'Domain'   => $cookie->domain,
                    'Path'     => $cookie->path,
                    'Expires'  => $cookie->expires
                ])
            );
        return $c;
    }

    private function post($uri, $params = [])
    {
        $params['nonce'] =(int)(microtime(true) * 10000);
        $res = $this->client->post($uri, [
            'headers'=>[
                'Key'=>$this->key,
                'Sign'=>$this->sign($params),
                'Accept'=>'application/json'
            ],
            'form_params'=>$params
        ]);
        return json_decode($res->getBody()->getContents(), true);
    }

    private function sign($params){
        $query = http_build_query($params);
        return hash_hmac('SHA512', $query, $this->secret);
    }

    public function getBalances()
    {
        if(($state = $this->getAccountState()) && $state->status === 'authorised')
            $this->saveCsrf();
        $info = $this->post('user_info');
        if(isset($info['error']) && strpos($info['error'], '40017')){
            $this->account->status()->associate(AccountStatus::where('name', 'error')->first());
            $this->account->description = $info['error'];
            $this->account->save();
        }
        $balances = [];
        foreach ($info['balances'] as $name => $balance){
            $name = isset($this->reverse_currencies[$name]) ? $this->reverse_currencies[$name] : $name;
            array_push($balances, [
                'name'=>$name,
                'balance'=>$balance
            ]);
        }
        return $balances;
    }

    public function getRates()
    {
        $res = $this->client->get('ticker');
        $res = json_decode($res->getBody()->getContents(), true);
        $rates = [];
        foreach($res as $name => $pair){
            $name = explode("_", $name);
            if($name[1] === 'BTC' || $name[1] === 'USD'){
                if(!($presist = @$rates[$name[0]]) || $presist['to'] !== 'BTC'){
                    if(isset($this->reverse_currencies[$name[0]]))
                        $name[0] = $this->reverse_currencies[$name[0]];
                    $rates[$name[0]] = [
                        'to'=>$name[1],
                        'rate'=>floatval($pair['sell_price'])
                    ];
                }
            } elseif($name[0] === 'USD' && $name[1] === 'RUB'){
                $rates['RUR'] = [
                    'to'=>'USD',
                    'rate'=>1/floatval($pair['sell_price'])
                ];
            }
        }
        return $rates;
    }


    public function getTransactionHistory()
    {
        $transactions = [];
        $balances = [];
        $now = Carbon::now();
        $till = ($last = $this->account->transactions()->latest('id')->first()) ? $last->date / 1000 : $this->account->created_at->timestamp;
        while($now->timestamp > $till){
            $res = $this->post('wallet_history',['date'=>$now->timestamp]);
            foreach($res['history'] as $tr){
                $deb = true;
                if($tr['type'] === 'deposit' || $tr['type'] === 'buy')
                    $deb = false;
                $name = isset($this->reverse_currencies[$tr['curr']]) ? $this->reverse_currencies[$tr['curr']] : $tr['curr'];
                if(!isset($balances[$name]))
                    $balances[$name] = $this->account->balances()->whereHas('currency', function($q) use($name){
                        $q->where('name', $name);
                    })->first();
                $balance = $balances[$name];
                if($balance){
                    $similar = $this->account->transactions()
                        ->where('info', 'like', "%account\":%\"{$tr['account']}\"%")
                        ->where('balance_id', $balance->id)
                        ->where('date', $tr['dt'] * 1000)
                        ->first();
                    if(!$similar){
                        array_push($transactions, new AccountTransaction([
                            'value'=>$tr['amount'],
                            'debit'=>$deb,
                            'balance_id'=>$balance->id,
                            'info'=>array_only($tr, ['provider', 'account']),
                            'date'=>$tr['dt'] * 1000
                        ]));
                    } else {
                        return $transactions;
                    }
                }
            }
            $now = $now->subDay();
        }
        return $transactions;
    }

    public function saveState()
    {
        $cookies_array = $this->client->getConfig('cookies');
        $carr = [];
        foreach($cookies_array as $cookie){
            array_push($carr, [
                'name'     => $cookie->getName(),
                'value'    => $cookie->getValue(),
                'domain'   => $cookie->getDomain(),
                'path'     => $cookie->getPath(),
                'expires'  => $cookie->getExpires()
            ]);
        }
        $cookie_setting = $this->account->settings()->where('name', 'cookies')->first();
        if($cookie_setting){
            $this->account->settings()->updateExistingPivot($cookie_setting->id, [
                'value'=>json_encode($carr)
            ]);
        }
        else{
            $this->account->settings()->attach(AccountSetting::where('name', 'cookies')->first()->id, [
                'value'=>json_encode($carr)
            ]);
        }
    }

    public function topUp()
    {
        $auth = $this->Auth();
        if($auth === true){
            $this->validate(request(), [
                'amount'=>'required',
                'currency'=>'required',
                'method'=>'required|in:0,1,2'
            ],[
                'required'=>'required',
                'in'=>'wrong'
            ]);
            $amount = request('amount');
            $currency = strtoupper(request('currency'));
            $providers = [
                'Payeer',
                'YandexMoney (DBB)',
                'Qiwi (DBB)'
            ];
            $provider = $providers[request('method')];
            if(isset($this->currencies[$currency]))
                $currency = $this->currencies[$currency];
            $state = $this->getAccountState(true);
            $response = json_decode($this->client->post('https://exmo.com/ctrl/fiat_deposite',[
                'json'=>[
                    'csrf_token'=>$state->csrf_token,
                    'currency'=>$currency,
                    'provider'=>$provider,
                    'summa'=>$amount,
                    'params'=>[
                        'amount'=>$amount
                    ]
                ]
            ])->getBody()->getContents(), true);
            if($response && $response['success']){
                Log::info('redirect to payment system (topup ' . $this->account->id . '): ' . $response['data']['url']);
                return redirect()->away($response['data']['url']);
            }
            else{
                $this->dropState();
                Log::info('cannot create payment request (topup ' . $this->account->id . ')');
                return $this->stateResponse('refresh');
            }
        } else if ($auth === false){
            $this->dropState();
            Log::info('auth failed (topup ' . $this->account->id . ')');
            return $this->stateResponse('refresh');
        }
        return $auth;
    }

    private function getCaptchaApi(){
        $api = new NoCaptcha();
        $api->setKey(config('anticaptcha.key'));
        $api->setUserAgent($this->ua);
        return $api;
    }

    private function saveCsrf(){
        $res = $this->client->get('https://exmo.com/wallet')->getBody()->getContents();
        $token = [];
        if(preg_match_all("/User.token='(.*)'/", $res, $token)){
            $token = $token[1][0];
            $this->setAccountState([
                'status'=>'authorised',
                'csrf_token'=>$token
            ]);
            $this->saveState();
            Log::info('found csrf-token (topup ' . $this->account->id . ')');
            return true;
        }
        Log::info('cant parse csrf (topup ' . $this->account->id . ')');
        return false;
    }

    private function dropState(){
        if(($state = AccountSetting::where('name', 'state')->first())) {
            $this->account->settings()->detach($state->id);
        }
        if($cookie_setting = $this->account->settings()->where('name', 'cookies')->first()){
            $this->account->settings()->detach($cookie_setting->id);
        }
    }

    public function Auth()
    {
        $state = $this->getAccountState();
        if($state){
            switch ($state->status){
                case "captcha":
                    if(time() > $state->until){
                        $this->dropState();
                        return false;
                    }
                    $api = $this->getCaptchaApi();
                    $api->setTaskId($state->ac_task);
                    if($api->waitForResult()){
                        Log::info('captcha solved (topup ' . $this->account->id . ')');
                        $solution = $api->getTaskSolution();
                        $response = json_decode($this->client->post('https://exmo.com/ctrl/userLogin',[
                            'headers'=>[
                                'Content-Type'=>'application/json;charset=UTF-8',
                                'Referer'=>'https://exmo.com/en/login'
                            ],
                            'json'=>[
                                'login'=>$this->email,
                                'password'=>$this->password,
                                'totp'=>'',
                                'recaptcha_response_field'=>$solution
                            ]
                        ])->getBody()->getContents(), true);
                        if($response['success'] && !$response['error']){
                            Log::info('logged in (topup ' . $this->account->id . ')');
                            return $this->saveCsrf();
                        } else if (strpos($response['error'], '10130') !== FALSE){
                            $this->dropState();
                            return $this->stateResponse('refresh');
                        } else {
                            $this->account->status()->associate(AccountStatus::where('name', 'error')->first());
                            $this->account->description = $response['error'];
                            $this->account->save();
                            return redirect(substr(request()->path(), 0, strpos(request()->path(), '/topup')))
                                ->withErrors([
                                    'wrong_credentials'
                                ]);
                        }
                    } elseif($api->getTaskStatus() !== 'processing') {
                        return redirect(substr(request()->path(), 0, strpos(request()->path(), '/topup')))
                            ->withErrors([
                                'captcha_api_error'
                            ]);
                    }
                    Log::info('captcha still processing (topup ' . $this->account->id . ')');
                    return $this->stateResponse('refresh');
                    break;
                case "authorised":
                    return $this->saveCsrf();
                default:
                    return false;
            }
        } else {
            if($this->authorised()){
                Log::info('authorised already (topup ' . $this->account->id . ')');
                return $this->saveCsrf();
            } else {
                Log::info('no auth (topup ' . $this->account->id . ')');
                $url = $this->getLoginPage();
                $ckey = $this->getCaptchaKey();
                if($ckey){
                    $api = $this->getCaptchaApi();
                    $api->setWebsiteURL($url);
                    $api->setWebsiteKey($ckey);

                    $proxy = [];

                    preg_match("/([^:]+):\/\/(([^:]+):([^@]+)@)?([^:]+):([0-9]+)/", $this->proxy, $proxy);

//                    $proxy = [
//                        '',
//                        'http',
//                        '',
//                        '0b50a589dc',
//                        '9558105b99',
//                        '194.242.27.19',
//                        33230
//                    ];

                    $api->setProxyType($proxy[1]);
                    $api->setProxyAddress($proxy[5]);
                    $api->setProxyPort($proxy[6]);
                    if($proxy[3]){
                        $api->setProxyLogin($proxy[3]);
                        $api->setProxyPassword($proxy[4]);
                    }
                    if (!$api->createTask()) {
                        return redirect(substr(request()->path(), 0, strpos(request()->path(), '/topup')))
                            ->withErrors([
                                'captcha_api_error'
                            ]);
                    }
                    $taskId = $api->getTaskId();
                    $this->setAccountState([
                        'status'=>'captcha',
                        'ac_task'=>$taskId,
                        'until'=>time()+(5*60)
                    ]);
                    $this->saveState();
                    return $this->stateResponse('refresh');
                }
                Log::info('cant find captcha key (topup ' . $this->account->id . ')');
                return false;
            }
        }
    }

    private function authorised(){
        $res = $this->client->get('https://exmo.com/wallet', ['allow_redirects'=>false]);
        $res = $this->client->get('https://exmo.com'.$res->getHeaderLine('location'), ['allow_redirects'=>false]);
        $loc = $res->getHeaderLine('location');
        return strpos($loc, 'login') === FALSE;
    }

    private function getWalletPage(){
        $res = $this->client->get('https://exmo.com/wallet');
    }

    private function getLoginPage(){
        $url = 'https://exmo.com/login';
        $res = $this->client->get($url, [
            'on_stats' => function (TransferStats $stats) use (&$url) {
                $url = $stats->getEffectiveUri();
            }
        ]);
        return $url->getScheme() . '://' . $url->getHost() . $url->getPath();
    }

    private function getCaptchaKey(){
        $res = $this->client->get('https://exmo.com/static/script.min.js')->getBody()->getContents();
        $output_array = [];
        if(preg_match_all("/self.public_key=\"([^\"]+)\"/", $res, $output_array)){
            if(count($output_array[0]))
                return $output_array[1][0];
        }
        return null;
    }

    private function getAccountState($force = false){
        if($this->account){
            if($this->state && !$force)
                return $this->state;
            if(($state = $this->account->settings()->where('name', 'state')->first()) && $state->info->value) {
                $this->state = $state->info->value;
                return $this->state;
            }
        }
        return null;
    }

    private function setAccountState($object){
        if(($state = AccountSetting::where('name', 'state')->first())) {
            $this->account->settings()->sync([
                $state->id=>[
                    'value'=>json_encode($object)
                ]
            ], false);
        }
        $this->state = $this->getAccountState(true);
    }
    private function stateResponse($state, $params = null){
        $p = [
            'state'=>$state
        ];
        if($params){
            array_merge($p, $params);
        }
        return view('layouts.proceeding', $p);
    }
}