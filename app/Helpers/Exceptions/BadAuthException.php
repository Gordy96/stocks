<?php
/**
 * Created by PhpStorm.
 * User: Johnny
 * Date: 04/06/2018
 * Time: 08:58
 */

namespace App\Helpers\Exceptions;


class BadAuthException extends \Exception {
    public $email;
    public $password;
    public function __construct($email, $password, $message = "", $code = 0, \Throwable $previous = null)
    {
        $this->email = $email;
        $this->password = $password;
        parent::__construct($message, $code, $previous);
    }
}