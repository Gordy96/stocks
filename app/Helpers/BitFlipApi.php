<?php
/**
 * Created by PhpStorm.
 * User: Johnny
 * Date: 09/06/2018
 * Time: 23:15
 */

namespace App\Helpers;


use App\Account;
use App\AccountSetting;
use App\AccountTransaction;
use GuzzleHttp\Client;

class BitFlipApi implements StockApiInterface
{

    /**
     * @var Client
     */
    private $client = null;

    /**
     * @var Account|null
     */

    private $account = null;

    const base = 'https://api.bitflip.cc/method/';

    private $key = '';
    private $secret = '';

    private $currencies = [
        'RUR'=>'RUB'
    ];

    private $reverse_currencies;

    public function __construct($account)
    {
        $defs = [
            'base_uri'=>static::base,
            'verify'=>false,
            'cookies'=>true,
            'headers'=>[
            ]
        ];

        $this->reverse_currencies = array_flip($this->currencies);

        if($account instanceof Account){
            $this->account = $account;

            $ua = $account->settings()->where('name', 'user-agent')->first();
            if(!$ua){
                $ua = RandomUserAgent::getRandomUserAgent();
                $setting = AccountSetting::where('name', 'user-agent')->first();
                $account->settings()->attach($setting->id, [
                    'value'=>$ua
                ]);
            } else {
                $ua = $ua->info->value;
            }

            $defs['headers']['User-Agent'] = $ua;

            $creds = $account->settings()->where('name', 'credentials')->first()->info->value;
            $this->key = $creds->key;
            $this->secret = $creds->secret;
            $proxy = $account->settings()->where('name', 'proxy')->first();

            if($proxy)
                $defs['proxy'] = $proxy->info->value;
        } elseif(is_string($account)){
            $defs['proxy'] = $account;
        }

        $this->client = new Client($defs);
    }

    public function getRates()
    {
        $res = $this->client->get('market.getRates');
        $res = json_decode($res->getBody()->getContents(), true);
        $rates = [];
        if($res[0] == null){
            foreach ($res[1] as $pair){
                $name = explode(":", $pair['pair']);
                if($name[1] === 'BTC' || $name[1] === 'USD'){
                    if(!($presist = @$rates[$name[0]]) || $presist['to'] !== 'BTC'){
                        $rates[$name[0]] = [
                            'to'=>$name[1],
                            'rate'=>floatval($pair['sell'])
                        ];
                    }
                }
            }
        }
        return $rates;
    }

    public function getTransactionHistory()
    {
        $transactions = [];
        $balances = $this->post('wallet.getBalances', ['version'=>'1.0']);
        foreach($balances[1] as $b){
            $res = $this->post('wallet.getTransactions', ['version'=>'1.0', 'id'=>$b['id'], 'locale'=>'en']);
            foreach($res[1] as $tr){
                $deb = true;
                if(strpos($tr['desc'], 'deposit') || strpos($tr['desc'], 'buy'))
                    $deb = false;
                $name = isset($this->reverse_currencies[$b['symbol']]) ? $this->reverse_currencies[$b['symbol']] : $b['symbol'];
                if(!isset($balances[$name]))
                    $balances[$name] = $this->account->balances()->whereHas('currency', function($q) use($name){
                        $q->where('name', $name);
                    })->first();
                $balance = $balances[$name];
                if($balance){
                    $similar = $this->account->transactions()
                        ->where('info', 'like', "%id\":%\"{$tr['id']}\"%")
                        ->where('balance_id',$balance->id)
                        ->where('date', $tr['timestamp'] * 1000)
                        ->first();
                    if(!$similar){
                        array_push($transactions, new AccountTransaction([
                            'value'=>$tr['amount'],
                            'debit'=>$deb,
                            'balance_id'=>$balance->id,
                            'info'=>array_only($tr, ['id', 'desc', 'state']),
                            'date'=>$tr['timestamp'] * 1000
                        ]));
                    } else {
                        break;
                    }
                } else {
                    break;
                }
            }
        }
        return $transactions;
    }

    public function getBalances()
    {
        $res = $this->post('wallet.getBalances', ['version'=>'1.0']);
        $balances = [];
        if($res[0] === null){
            foreach ($res[1] as $balance){
                array_push($balances, [
                    'name'=>$balance['symbol'],
                    'balance'=>$balance['amount']['available']
                ]);
            }
        }
        return $balances;
    }

    public function saveState()
    {
        // TODO: Implement saveState() method.
    }

    private function post($uri, $params)
    {
        $res = $this->client->post($uri, [
            'headers'=>[
                'X-API-Token'=>$this->key,
                'X-API-Sign'=>$this->sign($params),
                'Content-Type'=>'application/json'
            ],
            'json'=>$params
        ]);
        return json_decode($res->getBody()->getContents(), true);
    }

    private function sign($params){
        $query = json_encode($params);
        return hash_hmac('SHA512', $query, $this->secret);
    }

    public function topUp()
    {

    }

    public function Auth()
    {
        // TODO: Implement Auth() method.
    }
}