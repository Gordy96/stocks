<?php
/**
 * Created by PhpStorm.
 * User: Johnny
 * Date: 10/06/2018
 * Time: 11:29
 */

namespace App\Helpers;


use App\Account;
use App\AccountSetting;
use App\AccountTransaction;
use Carbon\Carbon;
use GuzzleHttp\Client;

class YobitApi implements StockApiInterface
{
    /**
     * @var Client
     */
    private $client = null;

    /**
     * @var Account|null
     */

    private $account = null;

    const base = 'https://yobit.net/tapi';

    private $key = '';
    private $secret = '';

    const point = 1528622311;

    public function __construct($account)
    {
        $defs = [
            'base_uri'=>static::base,
            'verify'=>false,
            'cookies'=>true,
            'headers'=>[
            ]
        ];

        if($account instanceof Account){
            $this->account = $account;

            $ua = $account->settings()->where('name', 'user-agent')->first();
            if(!$ua){
                $ua = RandomUserAgent::getRandomUserAgent();
                $setting = AccountSetting::where('name', 'user-agent')->first();
                $account->settings()->attach($setting->id, [
                    'value'=>$ua
                ]);
            } else {
                $ua = $ua->info->value;
            }

            $defs['headers']['User-Agent'] = $ua;

            $creds = $account->settings()->where('name', 'credentials')->first()->info->value;
            $this->key = $creds->key;
            $this->secret = $creds->secret;
            $proxy = $account->settings()->where('name', 'proxy')->first();

            if($proxy)
                $defs['proxy'] = $proxy->info->value;
        } elseif(is_string($account)){
            $defs['proxy'] = $account;
        }

        $this->client = new Client($defs);
    }

    public function getRates()
    {
        $res = $this->client->post('https://yobit.net/ajax/system_markets.php', [
            'form_params'=>[
                "method"=>"change_market_base",
                "csrf_token"=>"",
                "locale"=>"en",
                "base"=>"btc",
                "pid"=>"40001"
            ]
        ]);
        $res = json_decode($res->getBody()->getContents(), true);
        $rates = [];
        if($res['error'] == 0){
            $output_array = [];
            preg_match_all("/class=\"first\">([^<]+)<\/td>\s+[^>]+>([0-9.]+)</", $res['html'], $output_array);
            for($i = 0; $i < count($output_array[0]); $i++){
                $rates[$output_array[1][$i]] = [
                    'to'=>"BTC",
                    'rate'=>floatval($output_array[2][$i])
                ];
            }
        }
        $res = $this->client->get('/api/3/ticker/btc_usd');
        $res = json_decode($res->getBody()->getContents(), true);
        if(isset($res['btc_usd'])){
            $rates['BTC'] = [
                'to'=>'USD',
                'rate'=>floatval($res['btc_usd']['sell'])
            ];
        }
        return $rates;
    }

    private function post($uri, $params)
    {
        $params['method'] = $uri;
        if(!isset($params['nonce']))
            $params['nonce'] = time() - static::point;
        $res = $this->client->post('', [
            'headers'=>[
                'Key'=>$this->key,
                'Sign'=>$this->sign($params)
            ],
            'form_params'=>$params
        ]);
        return json_decode($res->getBody()->getContents(), true);
    }

    private function sign($params){
        $query = http_build_query($params);
        return hash_hmac('SHA512', $query, $this->secret);
    }

    public function saveState()
    {
        // TODO: Implement saveState() method.
    }

    public function getTransactionHistory()
    {
        $transactions = [];
        $balances = [];
        $from = ($last = $this->account->transactions()->latest('id')->first()) ? $last->info->id : 0;
        $res = $this->post('TradeHistory',['from'=>$from]);
        if(!$res['success'])
            return $transactions;
        foreach($res['return'] as $id => $tr){
            $deb = true;
            $type = strtolower($tr['type']);
            if($type === 'deposit' || $type === 'buy')
                $deb = false;
            $name = strtoupper(explode('_', $tr['pair'])[0]);
            $date = $tr['timestamp'] * 1000;
            if(!isset($balances[$name]))
                $balances[$name] = $this->account->balances()->whereHas('currency', function($q) use($name){
                    $q->where('name', $name);
                })->first();
            $balance = $balances[$name];
            if($balance){
                $similar = $this->account->transactions()
                    ->where('info', 'like', "%id\":%\"{$tr['id']}%")
                    ->where('balance_id',$balance->id)
                    ->where('date', $date)
                    ->first();
                if(!$similar){
                    array_push($transactions, new AccountTransaction([
                        'value'=>$tr['amount'],
                        'debit'=>$deb,
                        'balance_id'=>$balance->id,
                        'info'=>array_merge(array_only($tr, ['is_your_order','order_id', 'rate']), ["id"=>$id]),
                        'date'=>$tr['date']
                    ]));
                } else {
                    return $transactions;
                }
            }
        }
        return $transactions;
    }

    public function getBalances()
    {
        $res = $this->post('getInfo', []);
        $balances = [];
        if($res['success']){
            if(isset($res['return']['funds'])){
                foreach($res['return']['funds'] as $name => $balance){
                    array_push($balances, [
                        'name'=>$name,
                        'balance'=>floatval($balance)
                    ]);
                }
            }
        }
        return $balances;
    }

    public function topUp()
    {

    }
    public function Auth()
    {
        // TODO: Implement Auth() method.
    }
}