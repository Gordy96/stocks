<?php
/**
 * Created by PhpStorm.
 * User: Johnny
 * Date: 12/06/2018
 * Time: 20:58
 */

namespace App\Helpers\Captcha;


interface AntiCaptchaTaskProtocol
{
    public function getPostData();
    public function getTaskSolution();
}