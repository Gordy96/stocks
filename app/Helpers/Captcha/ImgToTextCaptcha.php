<?php
/**
 * Created by PhpStorm.
 * User: Johnny
 * Date: 13/06/2018
 * Time: 15:15
 */

namespace App\Helpers\Captcha;


class ImgToTextCaptcha extends NoCaptcha
{
    /**
     * @var string
     */
    private $image_body;
    /**
     * @return array
     */
    public function getPostData() {
        return array(
            "type"          =>  "ImageToTextTask",
            "body"          =>  $this->image_body,
            "phrase"        =>  false,
            "case"          =>  false,
            "numeric"       =>  false,
            "math"          =>  0,
            "minLength"     =>  0,
            "maxLength"     =>  0
        );
    }

    public function setImageBody($b64){
        $this->image_body = $b64;
    }

    public function getImageBody(){
        return $this->image_body;
    }

    public function getTaskSolution() {
        return $this->taskInfo->solution->text;
    }
}