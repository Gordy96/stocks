<?php

use Illuminate\Database\Seeder;

class RoleUserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \Illuminate\Support\Facades\DB::table('user_role')->insert([
            [
                'role_id'=>2,
                'user_id'=>1
            ]
        ]);
    }
}
