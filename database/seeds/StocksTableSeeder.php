<?php

use Illuminate\Database\Seeder;

class StocksTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \Illuminate\Support\Facades\DB::table('stocks')->insert([
            [
                'name'=>"WexNZ",
                'executor'=>'\App\Helpers\WexNzApi'
            ],[
                'name'=>"BitFlip",
                'executor'=>'\App\Helpers\BitFlipApi'
            ],[
                'name'=>"Exmo",
                'executor'=>'\App\Helpers\ExmoApi'
            ],[
                'name'=>"LiveCoin",
                'executor'=>'\App\Helpers\LiveCoinApi'
            ],[
                'name'=>"Yobit",
                'executor'=>'\App\Helpers\YobitApi'
            ]
        ]);
    }
}
