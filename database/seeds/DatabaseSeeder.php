<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
         $this->call(UsersTableSeeder::class);
         $this->call(RolesTableSeeder::class);
         $this->call(RoleUserTableSeeder::class);
         $this->call(StocksTableSeeder::class);
//         $this->call(CurrenciesTableSeeder::class);
//         $this->call(StockCurrencyTableSeeder::class);
         $this->call(AccountStatusesTableSeeder::class);
         $this->call(AccountSettingsTableSeeder::class);
//         $this->call(AccountsTableSeeder::class);
//         $this->call(AccountBalancesTableSeeder::class);
    }
}
