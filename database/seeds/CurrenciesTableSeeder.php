<?php

use Illuminate\Database\Seeder;

class CurrenciesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \Illuminate\Support\Facades\DB::table('currencies')->insert([
            ['name'=>'USDT'],
            ['name'=>'BTC'],
        ]);
    }
}
