<?php

use Illuminate\Database\Seeder;

class StockCurrencyTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \Illuminate\Support\Facades\DB::table('stock_currency')->insert([
            [
                'stock_id'=>1,
                'currency_id'=>1,
                'rate'=>0
            ],
            [
                'stock_id'=>1,
                'currency_id'=>2,
                'rate'=>0
            ],
        ]);
    }
}
