<?php

use Illuminate\Database\Seeder;

class AccountSettingsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \Illuminate\Support\Facades\DB::table('account_settings')->insert([
            [
                'name'=>'proxy',
                'type'=>'string'
            ],[
                'name'=>'credentials',
                'type'=>'object'
            ],[
                'name'=>'cookies',
                'type'=>'object'
            ],[
                'name'=>'user-agent',
                'type'=>'string'
            ],[
                'name'=>'state',
                'type'=>'object'
            ]
        ]);
    }
}
