<?php
return [
    'title'=>'Stocks',
    'table'=>[
        'name'=>'Stock name',
        'total'=>'Total balance',
        'date'=>'Creation date'
    ],
    'currencies'=>[
        'table'=>[
            'name'=>'Currency',
            'rate'=>'Exchange rate',
            'change'=>''
        ]
    ],
    'last_updated'=>'Last updated',
    'name'=>'Name'
];