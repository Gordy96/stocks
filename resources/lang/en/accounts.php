<?php
return [
    'table'=>[
        'id'=>'Account id',
        'stock'=>'Stock',
        'active_purses'=>'Active purses',
        'balance'=>'Approx. balance',
        'date'=>'Creation date',
        'owner'=>'Owner'
    ],
    'total_balance'=>'Total balance',
    'title'=>'All accounts',
    'total_transactions'=>'Total amount of transactions',
    'balances'=>[
        'table'=>[
            'currency'=>'Purse',
            'value'=>'Balance',
            'date'=>'Last update'
        ]
    ],
    'single_title'=>'Account'
];