<?php
return [
    'stocks'=>'Stocks',
    'users'=>'Users',
    'accounts'=>'Accounts',
    'transactions'=>'Transactions',
    'cabinet'=>'Cabinet'
];