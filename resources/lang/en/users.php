<?php
return [
    'users_accounts'=>':user`s stock accounts',
    'total_balance'=>'Total balance',
    'total_accounts'=>'Total accounts',
    'total_transactions'=>'Total transactions',
    'title'=>'Users',
    'table'=>[
        'id'=>'User id',
        'name'=>'User name',
        'email'=>'User email',
        'total'=>'Total balance',
        'date'=>'Registration date'
    ]
];