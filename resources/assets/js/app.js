
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

// window.Vue = require('vue');
//
// /**
//  * Next, we will create a fresh Vue application instance and attach it to
//  * the page. Then, you may begin adding components to this application
//  * or customize the JavaScript scaffolding to fit your unique needs.
//  */
//
// Vue.component('example-component', require('./components/ExampleComponent.vue'));
//
// const app = new Vue({
//     el: '#app'
// });

window.utils = {};
window.gui = {};

window.utils.goto = function (address){
    window.location = address;
};


window.gui.makeTabs = function (classname){
    var tabs = document.querySelectorAll('.' + classname +' .tabs a[role=tab]');
    tabs.forEach(function(e){
        e.onclick = function(event){
            var self = this;
            tabs.forEach(function(t){
                t === self ? t.parentNode.classList.add('is-active') : t.parentNode.classList.remove('is-active')
            });
            var current = document.getElementById(e.attributes['aria-controls'].value);
            for(var i = 0; i < current.parentNode.children.length; i++){
                var c = current.parentNode.children[i];
                if(c !== current)
                    c.classList.add('is-hidden');
            }
            current.classList.remove('is-hidden');
        }
    });
};

// window.gui.remoteDeleteRow()