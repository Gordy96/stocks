@extends("layouts.admin")
@section("body")
    <section class="section">
        <div class="card">
            <header class="card-header">
                <p class="card-header-title">
                    @lang('accounts.title')
                </p>
            </header>
            <div class="card-content">
                <nav class="level">
                    <div class="level-item has-text-centered">
                        <div>
                            <p class="heading">@lang('accounts.total_balance')</p>
                            <p class="title">@include('components.approx_usd',['value'=>\App\Account::sum('total')])</p>
                        </div>
                    </div>
                    <div class="level-item has-text-centered">
                        <div>
                            <p class="heading">@lang('users.total_transactions')</p>
                            <p class="title">{{ \App\AccountTransaction::count() }}</p>
                        </div>
                    </div>
                </nav>
                <table class="table is-bordered is-striped is-narrow is-hoverable is-fullwidth">
                    <thead>
                    <tr>
                        <th>@lang('accounts.table.id')</th>
                        <th>@lang('accounts.table.balance')</th>
                        <th>@lang('accounts.table.owner')</th>
                        <th>@lang('accounts.table.status')</th>
                        <th>@lang('accounts.table.date')</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($accounts as $account)
                        <tr>
                            <td>
                                <a href="/admin/accounts/{{$account->id}}">{{$account->id}}</a>
                            </td>
                            <td>
                                @include('components.approx_usd',['value'=>$account->total])
                            </td>
                            <td>
                                <a href="/admin/users/{{$account->user->id}}">{{$account->user->email}}</a>
                            </td>
                            <td>{{$account->status->name}}</td>
                            <td>{{$account->created_at}}</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
                {{$accounts->links("components.pagination.default")}}
            </div>
        </div>
    </section>
    @parent
@endsection