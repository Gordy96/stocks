@extends("layouts.admin")
@section("body")
    <section class="section">
        <div class="card">
            <header class="card-header">
                <p class="card-header-title">
                    &nbsp;
                </p>
            </header>
            <div class="card-content">
                <nav class="level">
                    <div class="level-item has-text-centered">
                        <div>
                            <p class="heading">@lang('accounts.total')</p>
                            <p class="title">@include('components.approx_usd', ['value'=>$account->total])</p>
                        </div>
                    </div>
                    <div class="level-item has-text-centered">
                        <div>
                            <p class="heading">@lang('accounts.total_transaction')</p>
                            <p class="title">{{$account->transactions()->count()}}</p>
                        </div>
                    </div>
                    <div class="level-item has-text-centered">
                        <div>
                            <p class="heading">@lang('accounts.stock_name')</p>
                            <p class="title">{{$account->stock->name}}</p>
                        </div>
                    </div>
                </nav>
                <table class="table is-bordered is-striped is-narrow is-hoverable is-fullwidth has-cursor-pointer">
                    <thead>
                    <tr>
                        <th>@lang('accounts.balances.table.currency')</th>
                        <th>@lang('accounts.balances.table.value')</th>
                        <th>@lang('accounts.balances.table.date')</th>
                    </tr>
                    </thead>
                    <tbody>
                    @php($balances = $account->balances()->where('value', '>', 0)->paginate())
                    @foreach($balances as $balance)
                        <tr onclick="utils.goto('{{'/admin/balances/' .$balance->id}}')">
                            <td>
                                <span class="tag is-primary">{{$balance->currency->name}}</span>
                            </td>
                            <td>{{$balance->value}}</td>
                            <td>
                                {{$balance->updated_at}}
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
                {{$balances->links("components.pagination.default")}}
            </div>
        </div>
    </section>
    @parent
@endsection