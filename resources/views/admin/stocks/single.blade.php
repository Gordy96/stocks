@extends("layouts.admin")
@section("body")
    <section class="section">
        <div class="card">
            <header class="card-header">
                <p class="card-header-title">
                    @lang('stocks.title')
                </p>
            </header>
            <div class="card-content">
                <nav class="level">
                    <div class="level-item has-text-centered">
                        <div>
                            <p class="heading">@lang('stocks.name')</p>
                            <p class="title">{{$stock->name}}</p>
                        </div>
                    </div>
                    <div class="level-item has-text-centered">
                        <div>
                            <p class="heading">@lang('stocks.last_updated')</p>
                            <p class="title">{{$stock->updated_at}}</p>
                        </div>
                    </div>
                </nav>
                <table class="table is-bordered is-striped is-narrow is-hoverable is-fullwidth">
                    <thead>
                    <tr>
                        <th>@lang('stocks.currencies.table.name')</th>
                        <th>@lang('stocks.currencies.table.rate')</th>
                        <th>@lang('stocks.currencies.table.change')</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($stock->currencies()->where('name', '!=', 'USD')->get() as $currency)
                        <tr>
                            <td>
                                1 "{{$currency->name}}"
                            </td>
                            <td>{{number_format($currency->pivot->rate, $currency->pivot->virtual ? 8 : 2)}} {{$currency->pivot->virtual ? 'BTC' : 'USD'}}</td>
                            <td>
                                @if($currency->pivot->change_rate > 0)
                                    <span class="tag is-success">
                                @elseif($currency->pivot->change_rate < 0)
                                    <span class="tag is-danger">
                                @else
                                    <span class="tag">
                                @endif
                                    {{$currency->pivot->change_rate > 0 ? '+' : ''}}{{round($currency->pivot->change_rate, 2)}}%
                                </span>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </section>
    @parent
@endsection