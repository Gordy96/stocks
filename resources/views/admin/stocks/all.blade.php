@extends("layouts.admin")
@section("body")
    <section class="section">
        <div class="card">
            <header class="card-header">
                <p class="card-header-title">
                    @lang('stocks.title')
                </p>
            </header>
            <div class="card-content">
                <table class="table is-bordered is-striped is-narrow is-hoverable is-fullwidth">
                    <thead>
                    <tr>
                        {{--<th>@lang('stocks.id')</th>--}}
                        <th>@lang('stocks.table.name')</th>
                        <th>@lang('stocks.table.total')</th>
                        <th>@lang('stocks.table.date')</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($stocks as $stock)
                        <tr>
                            {{--<td>{{$stock->id}}</td>--}}
                            <td><a href="/admin/stocks/{{$stock->id}}">{{$stock->name}}</a></td>
                            <td>@include("components.approx_usd",['value'=>$stock->accounts()->sum('total')])</td>
                            <td>{{$stock->created_at}}</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
                {{$stocks->links("components.pagination.default")}}
            </div>
        </div>
    </section>
    @parent
@endsection