@extends("layouts.admin")
@section("body")
    <section class="section">
        <div class="card">
            <header class="card-header">
                <p class="card-header-title">
                    @lang('users.users_accounts', ['user'=>$user->name])
                </p>
            </header>
            <div class="card-content">
                <nav class="level">
                    <div class="level-item has-text-centered">
                        <div>
                            <p class="heading">@lang('users.total_balance')</p>
                            <p class="title">@include('components.approx_usd', ['value'=>$user->accounts()->sum('total')])</p>
                        </div>
                    </div>
                    <div class="level-item has-text-centered">
                        <div>
                            <p class="heading">@lang('users.total_accounts')</p>
                            <p class="title">{{ $user->accounts()->count() }}</p>
                        </div>
                    </div>
                    <div class="level-item has-text-centered">
                        <div>
                            <p class="heading">@lang('users.total_transactions')</p>
                            <p class="title">{{ $user->transactions()->count() }}</p>
                        </div>
                    </div>
                </nav>
                @php($accounts = $user->accounts()->with(['balances', 'stock'])->paginate())
                <table class="table is-bordered is-striped is-narrow is-hoverable is-fullwidth has-cursor-pointer">
                    <thead>
                    <tr>
                        <th style="max-width: 60px">@lang('accounts.table.id')</th>
                        <th>@lang('accounts.table.stock')</th>
                        <th>@lang('accounts.table.active_purses')</th>
                        <th>@lang('accounts.table.balance')</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($accounts as $account)
                        <tr onclick="utils.goto('/cabinet/accounts/{{$account->id}}')">
                            <td style="max-width: 60px">
                                <a href="/admin/accounts/{{$account->id}}">{{$account->id}}</a>
                            </td>
                            <td>{{$account->stock->name}}</td>
                            <td>
                                @foreach($account->balances()->where('value', '>', 0)->get() as $balance)
                                    <span class="tag is-primary">
                                        {{$balance->currency->name}}
                                    </span>
                                @endforeach
                            </td>
                            <td>@include('components.approx_usd', ['value'=>$account->total])</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
                {{$accounts->links("components.pagination.default")}}
            </div>
        </div>
    </section>
    @parent
@endsection