@extends("layouts.admin")
@section("body")
    <section class="section">
        <div class="card">
            <header class="card-header">
                <p class="card-header-title">
                    @lang('users.title')
                </p>
            </header>
            <div class="card-content">
                <table class="table is-bordered is-striped is-narrow is-hoverable is-fullwidth">
                    <thead>
                    <tr>
                        <th>@lang('users.table.id')</th>
                        <th>@lang('users.table.email')</th>
                        <th>@lang('users.table.name')</th>
                        <th>@lang('users.table.roles')</th>
                        <th>@lang('users.table.date')</th>
                        <th>@lang('users.table.total')</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($users as $user)
                        <tr>
                            <td>{{$user->id}}</td>
                            <td><a href="/admin/users/{{$user->id}}">{{$user->email}}</a></td>
                            <td><a href="/admin/users/{{$user->id}}">{{$user->name}}</a></td>
                            <td>
                                @foreach($user->roles as $role)
                                    <span class="tag is-info">{{$role->name}}</span>
                                @endforeach
                            </td>
                            <td>{{$user->created_at}}</td>
                            <td>@include('components.approx_usd', ['value'=>$user->accounts()->sum('total')])</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
                {{$users->links("components.pagination.default")}}
            </div>
        </div>
    </section>
    @parent
@endsection