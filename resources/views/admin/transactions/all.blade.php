@extends("layouts.admin")
@section("body")
    <section class="section">
        <div class="card">
            <header class="card-header">
                <p class="card-header-title">
                    Component
                </p>
                <a href="#" class="card-header-icon" aria-label="more options">
                    <span class="icon">
                        <i class="fa fa-angle-down" aria-hidden="true"></i>
                    </span>
                </a>
            </header>
            <div class="card-content">
                <nav class="level">
                    <div class="level-item has-text-centered">
                        <div>
                            <p class="heading">@lang('accounts.balance')</p>
                            <p class="title">{{ \App\AccountBalance::sum('value') }}</p>
                        </div>
                    </div>
                    <div class="level-item has-text-centered">
                        <div>
                            <p class="heading">@lang('users.total_transactions')</p>
                            <p class="title">{{ \App\AccountTransaction::count() }}</p>
                        </div>
                    </div>
                </nav>
                <table class="table is-bordered is-striped is-narrow is-hoverable is-fullwidth">
                    <thead>
                    <tr>
                        <th>Id</th>
                        <th>Value</th>
                        <th>Account</th>
                        <th>Stock</th>
                        <th>Owner</th>
                        <th>Date</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($transactions as $transaction)
                        <tr>
                            <td>
                                <a href="/admin/transactions/{{$transaction->id}}">{{$transaction->id}}</a>
                            </td>
                            <td>
                                @if(!$transaction->debit)
                                    <span class="icon has-text-success">
                                        <i class="fa fa-arrow-up"></i>
                                        {{$transaction->value}}
                                    </span>
                                @else
                                    <span class="icon has-text-danger">
                                        <i class="fa fa-arrow-down"></i>
                                        {{$transaction->value}}
                                    </span>
                                @endif
                            </td>
                            <td>
                                <a href="/admin/accounts/{{$transaction->account->id}}">{{$transaction->account->id}}</a>
                            </td>
                            <td>
                                <a href="/admin/stocks/{{$transaction->account->stock->id}}">{{$transaction->account->stock->id}}</a>
                            </td>
                            <td>
                                <a href="/admin/users/{{$transaction->account->user->id}}">{{$transaction->account->user->email}}</a>
                            </td>
                            <td>{{$transaction->created_at}}</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
                {{$transactions->links("components.pagination.default")}}
            </div>
            <footer class="card-footer">
                <a href="#" class="card-footer-item">Save</a>
                <a href="#" class="card-footer-item">Edit</a>
                <a href="#" class="card-footer-item">Delete</a>
            </footer>
        </div>
    </section>
    @parent
@endsection