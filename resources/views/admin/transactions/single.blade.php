@extends("layouts.admin")
@section("body")
    <section class="section">
        <div class="card">
            <header class="card-header">
                <p class="card-header-title">
                    Component
                </p>
                <a href="#" class="card-header-icon" aria-label="more options">
                    <span class="icon">
                        <i class="fa fa-angle-down" aria-hidden="true"></i>
                    </span>
                </a>
            </header>
            <div class="card-content">
                <nav class="level">
                    <div class="level-item has-text-centered">
                        <div>
                            <p class="heading">@lang('transactions.value')</p>
                            @if(!$transaction->debit)
                                <span class="title icon has-text-success">
                                        <i class="fa fa-arrow-up"></i>
                                    {{$transaction->value}}
                                    </span>
                            @else
                                <span class="title icon has-text-danger">
                                        <i class="fa fa-arrow-down"></i>
                                    {{$transaction->value}}
                                    </span>
                            @endif
                        </div>
                    </div>
                    <div class="level-item has-text-centered">
                        <div>
                            <p class="heading">@lang('transactions.date')</p>
                            <p class="title">{{$transaction->created_at}}</p>
                        </div>
                    </div>
                </nav>
                @foreach($transaction->info as $k => $v)
                <p>{{$k}}</p>-><p>{{$v}}</p>
                @endforeach
            </div>
            <footer class="card-footer">
                <a href="#" class="card-footer-item">Save</a>
                <a href="#" class="card-footer-item">Edit</a>
                <a href="#" class="card-footer-item">Delete</a>
            </footer>
        </div>
    </section>
    @parent
@endsection