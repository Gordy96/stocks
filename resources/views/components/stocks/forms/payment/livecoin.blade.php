<form method="get" action="/cabinet/accounts/{{$account->id}}/topup" target="_">
    {{--{{csrf_field()}}--}}
    <div class="field is-horizontal">
        <div class="field-label">
            <label class="label">Phone</label>
        </div>
        <div class="field-body">
            <div class="field">
                <p class="control is-expanded has-icons-left">
                    <input class="input" type="text" placeholder="Phone" name="phone">
                    <span class="icon is-small is-left">
                        <i class="fa fa-user"></i>
                    </span>
                </p>
            </div>
        </div>
    </div>
    <div class="field is-horizontal">
        <div class="field-label"></div>
        <div class="field-body">
            <div class="field is-expanded">
                <div class="field has-addons">
                    <p class="control">
                        <span class="select">
                            <select name="currency">
                                <option value="usd">USD</option>
                                <option value="rur">RUR</option>
                                <option value="btc">BTC</option>
                            </select>
                        </span>
                    </p>
                    <p class="control is-expanded">
                        <input class="input" type="text" name="amount" placeholder="@lang('accounts.amount')">
                    </p>
                </div>
                <p class="help" style="width: 100%;">Komissia 10 procentov yoba</p>
            </div>
        </div>
    </div>
    <div class="field is-horizontal">
        <div class="field-label">
        </div>
        <div class="field-body">
            <div class="field">
                <div class="control">
                    <input type="submit" class="button is-primary" value="@lang('accounts.top_up')"/>
                </div>
            </div>
        </div>
    </div>
</form>