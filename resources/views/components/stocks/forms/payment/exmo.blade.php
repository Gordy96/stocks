<form method="get" action="/cabinet/accounts/{{$account->id}}/topup" target="_blank">
{{--    {{csrf_field()}}--}}
    <div class="field has-addons">
        <p class="control">
            <span class="select">
                <select name="method">
                    <option value="0">Payeer</option>
                    <option value="1">YandexMoney</option>
                    <option value="2">Qiwi</option>
                </select>
            </span>
        </p>
        <p class="control">
            <span class="select">
                <select name="currency">
                    <option value="usd">USD</option>
                    <option value="rur">RUR</option>
                    <option value="btc">BTC</option>
                </select>
            </span>
        </p>
        <p class="control">
            <input class="input" type="text" name="amount" placeholder="@lang('accounts.amount')">
        </p>
        <p class="control">
            <input type="submit" class="button is-primary" value="@lang('accounts.top_up')"/>
        </p>
    </div>
</form>