@extends("layouts.auth")
@section("body")

    <section class="hero is-light is-fullheight">
        <div class="hero-body">
            <div class="container">
                <div class="columns is-centered">
                    <article class="card is-rounded">
                        <div class="card-content">
                            <form method="post">
                                {{csrf_field()}}
                                <div class="field">
                                    <p class="control has-icons-left has-icons-right">
                                        <input name="email" class="input" type="email" placeholder="Email">
                                        <span class="icon is-small is-left">
                                        <i class="fa fa-envelope"></i>
                                    </span>
                                    </p>
                                </div>
                                <div class="field">
                                    <p class="control has-icons-left">
                                        <input name="password" class="input" type="password" placeholder="Password">
                                        <span class="icon is-small is-left">
                                        <i class="fa fa-lock"></i>
                                    </span>
                                    </p>
                                </div>
                                <div class="field">
                                    <p class="control">
                                        <input type="submit" class="button is-primary" value="Sign in">
                                        <a class="button" href="/register">Sign up</a>
                                    </span>
                                    </p>
                                </div>
                            </form>
                        </div>
                    </article>
                </div>
            </div>
        </div>
    </section>
@parent
@endsection