@extends("layouts.user")
@section('styles')
    @parent
@endsection
@section("body")
    <section class="section">
        <article class="card">
            <div class="card-header">
                <p class="card-header-title">@lang('settings')</p>
            </div>
            <div class="card-content">
                <p class="title">Password reset</p>
                <form method="post" action="/cabinet/password/reset">
                    @if($errors->has('password_old'))
                        <div class="field">
                            <label class="label">Old Password</label>
                            <div class="control">
                                <input class="input is-danger" type="password" placeholder="Old password" name="password_old">
                            </div>
                        </div>
                        <p class="help is-danger">@lang('password_reset.wrong_old')</p>
                    @else
                        <div class="field">
                            <label class="label">Old Password</label>
                            <div class="control">
                                <input class="input" type="password" placeholder="Old password" name="password_old">
                            </div>
                        </div>
                    @endif
                    @if($errors->has('password'))
                        <div class="field">
                            <label class="label">New Password</label>
                            <div class="control">
                                <input class="input is-danger" type="password" placeholder="New password" name="password">
                            </div>
                        </div>
                        <p class="help is-danger">{{$errors->first('password')}}</p>
                    @else
                        <div class="field">
                            <label class="label">New Password</label>
                            <div class="control">
                                <input class="input" type="password" placeholder="New password" name="password">
                            </div>
                        </div>
                    @endif
                    <div class="field">
                        <label class="label">Confirm Password</label>
                        <div class="control">
                            <input class="input" type="password" placeholder="Confirm password" name="password_confirmation">
                        </div>
                    </div>
                    <div class="field">
                        <p class="control">
                            <button class="button is-success" type="submit">
                                Reset
                            </button>
                        </p>
                    </div>
                    {{csrf_field()}}
                </form>
            </div>
        </article>
        <br>
        <article class="card">
            <div class="card-header">
                <p class="card-header-title">@lang('notifications')</p>
            </div>
            <div class="card-content">
                <article class="message">
                    <div class="message-header">
                        <p>Hello World</p>
                        <button class="delete" aria-label="delete"></button>
                    </div>
                    <div class="message-body">
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. <strong>Pellentesque risus mi</strong>, tempus quis placerat ut, porta nec nulla. Vestibulum rhoncus ac ex sit amet fringilla. Nullam gravida purus diam, et dictum <a>felis venenatis</a> efficitur. Aenean ac <em>eleifend lacus</em>, in mollis lectus. Donec sodales, arcu et sollicitudin porttitor, tortor urna tempor ligula, id porttitor mi magna a neque. Donec dui urna, vehicula et sem eget, facilisis sodales sem.
                    </div>
                </article>
            </div>
        </article>
    </section>
    @parent
@endsection