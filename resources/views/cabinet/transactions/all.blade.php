@extends("layouts.user")
@section("body")
    <section class="section">
        <div class="card">
            <header class="card-header">
                <p class="card-header-title">
                    @lang('balance.transactions')
                </p>
            </header>
            <div class="card-content">
                <nav class="level">
                    <div class="level-item has-text-centered">
                        <div>
                            <p class="heading">@lang('balance.total')</p>
                            <p class="title">{{ $balance->value }}</p>
                        </div>
                    </div>
                    <div class="level-item has-text-centered">
                        <div>
                            <p class="heading">@lang('balance.total_transactions')</p>
                            <p class="title">{{ $balance->transactions()->count() }}</p>
                        </div>
                    </div>
                    <div class="level-item has-text-centered">
                        <div>
                            <p class="heading">@lang('balance.currency')</p>
                            <p class="title">{{ $balance->currency->name }}</p>
                        </div>
                    </div>
                    <div class="level-item has-text-centered">
                        <div>
                            <p class="heading">@lang('accounts.stock_name')</p>
                            <p class="title"><a href="/cabinet/stocks/{{$balance->account->stock_id}}">{{$balance->account->stock->name}}</a></p>
                        </div>
                    </div>
                </nav>
                <table class="table is-bordered is-striped is-narrow is-hoverable is-fullwidth">
                    <thead>
                    <tr>
                        <th>@lang('transactions.id')</th>
                        <th>@lang('transactions.value')</th>
                        <th>@lang('transactions.date')</th>
                    </tr>
                    </thead>
                    <tbody>
                    @php($transactions = $balance->transactions()->paginate())
                    @foreach($transactions as $transaction)
                        <tr>
                            <td>
                                {{$transaction->id}}
                            </td>
                            <td>
                                @if(!$transaction->debit)
                                    <span class="has-text-success">
                                        <i class="fa fa-arrow-up"></i>
                                        {{$transaction->value}}
                                    </span>
                                @else
                                    <span class="has-text-danger">
                                        <i class="fa fa-arrow-down"></i>
                                        {{$transaction->value}}
                                    </span>
                                @endif
                            </td>
                            <td>{{\Carbon\Carbon::createFromTimestamp($transaction->date/1000)}}</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
                {{$transactions->links("components.pagination.default")}}
            </div>
        </div>
    </section>
    @parent
@endsection