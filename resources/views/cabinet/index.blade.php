@extends("layouts.user")
@section('styles')
    @parent
@endsection
@section("body")
    <section class="section">
        <div class="card">
            <header class="card-header">
                <p class="card-header-title">
                    @lang('cabinet.title')
                </p>
            </header>
            <div class="card-content">
                <nav class="level">
                    <div class="level-item has-text-centered">
                        <div>
                            <p class="heading">@lang('cabinet.total_accounts')</p>
                            <p class="title">{{Auth::user()->accounts()->count()}}</p>
                        </div>
                    </div>
                    <div class="level-item has-text-centered">
                        <div>
                            <p class="heading">@lang('cabinet.total_balance')</p>
                            <p class="title">@include("components.approx_balance")</p>
                        </div>
                    </div>
                </nav>
            </div>
        </div>
    </section>
    @parent
@endsection
@section('scripts')
    @parent
    <script>
        gui.makeTabs('tabs-container');
    </script>
@endsection