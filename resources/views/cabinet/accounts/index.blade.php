@extends("layouts.user")
@section('styles')
    @parent
@endsection
@section("body")
    <section class="section">
        <div class="card">
            <header class="card-header">
                <p class="card-header-title">
                    Accounts
                </p>
            </header>
            <div class="card-content">
                <table class="table is-bordered is-striped is-narrow is-hoverable is-fullwidth has-cursor-pointer">
                    <thead>
                    <tr>
                        <th style="max-width: 60px">@lang('accounts.table.id')</th>
                        <th>@lang('accounts.table.stock')</th>
                        <th>@lang('accounts.table.active_purses')</th>
                        <th>@lang('accounts.table.balance')</th>
                        <th>@lang('accounts.table.status')</th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($accounts as $account)
                        <tr onclick="utils.goto('/cabinet/accounts/{{$account->id}}')">
                            <td style="max-width: 60px">
                                <a href="/cabinet/accounts/{{$account->id}}">{{$account->id}}</a>
                            </td>
                            <td>
                                <a href="/cabinet/stocks/{{$account->stock_id}}">{{$account->stock->name}}</a>
                            </td>
                            <td>
                                @foreach($account->balances()->where('value', '>', 0)->get() as $balance)
                                    <span class="tag is-primary">
                                        {{$balance->currency->name}}
                                    </span>
                                @endforeach
                            </td>
                            <td>@include('components.approx_usd', ['value'=>$account->total])</td>
                            <td>{{$account->status->name}}</td>
                            <td>
                                <form method="get" action="/cabinet/accounts/{{$account->id}}/delete">
                                    <input type="submit" class="button is-warning" value="@lang('accounts.delete')">
                                </form>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
                {{$accounts->links("components.pagination.default")}}
            </div>
        </div>
        <br>
        <div class="card">
            <header class="card-header">
                <p class="card-header-title">
                    Add new
                </p>
            </header>
            <div class="card-content">
                @php($stocks = \App\Stock::all())
                @if($stocks)
                <div class="tabs-container">
                    <div class="tabs is-centered is-boxed">
                        <ul>
                            <li class="is-active">
                                <a id="tab{{$stocks[0]->id}}" aria-controls="panel{{$stocks[0]->id}}" role="tab">
                                    {{$stocks[0]->name}}
                                </a>
                            </li>
                            @for($i = 1; $i < count($stocks); $i++)
                                <li>
                                    <a id="tab{{$stocks[$i]->id}}" aria-controls="panel{{$stocks[$i]->id}}" role="tab">
                                        {{$stocks[$i]->name}}
                                    </a>
                                </li>
                            @endfor
                        </ul>
                    </div>
                    <div class="tabs-content">
                        <div id="panel{{$stocks[0]->id}}" aria-labelledby="tab{{$stocks[0]->id}}" role="tabpanel">
                            @if(View::exists('components.stocks.forms.'.strtolower($stocks[0]->name)))
                                @include('components.stocks.forms.'.strtolower($stocks[0]->name),['stock'=>$stocks[0]])
                            @endif
                        </div>
                        @for($i = 1; $i < count($stocks); $i++)
                            <div id="panel{{$stocks[$i]->id}}" aria-labelledby="tab{{$stocks[$i]->id}}" role="tabpanel" class="is-hidden">
                                @if(View::exists('components.stocks.forms.'.strtolower($stocks[$i]->name)))
                                    @include('components.stocks.forms.'.strtolower($stocks[$i]->name),['stock'=>$stocks[$i]])
                                @endif
                            </div>
                        @endfor
                    </div>
                </div>
                @endif
            </div>
        </div>
    </section>
    @parent
@endsection
@section('scripts')
    @parent
    <script>
        gui.makeTabs('tabs-container');
    </script>
@endsection