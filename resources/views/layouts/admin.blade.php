<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>CHECKCRYPT</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">
        @section('styles')
            <link href="{{asset('css/app.css')}}" rel="stylesheet" type="text/css">
        @show
    </head>

    <body class="has-background-light">
        <nav class="navbar has-shadow">
            <div class="container is-fluid">
                <div class="navbar-brand">
                    <a class="navbar-item" href="../">
                        <img src="{{asset('img/logo.png')}}" alt="Bulma: a modern CSS framework based on Flexbox">
                    </a>

                    <div class="navbar-burger burger" data-target="navMenu">
                        <span></span>
                        <span></span>
                        <span></span>
                    </div>
                </div>

                <div class="navbar-menu">
                    <div class="navbar-end">
                        <div class="navbar-item">
                            <strong>Total: </strong>&nbsp;<span>@include("components.approx_balance")</span>
                        </div>
                        <div class="navbar-item has-dropdown is-hoverable">
                            <a class="navbar-link">
                                {{\Illuminate\Support\Facades\Auth::user()->name}}
                            </a>

                            <div class="navbar-dropdown">
                                <a class="navbar-item" href="/cabinet">
                                    Cabinet
                                </a>
                                <a class="navbar-item" href="/cabinet/settings">
                                    Settings
                                </a>
                                <hr class="navbar-divider">
                                <a class="navbar-item" href="/logout">
                                    Logout
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </nav>
        <div class="container is-fluid">
            <section class="section">
                <div class="columns">
                <div class="column is-2 aside hero is-fullheight">
                    <aside class="menu">
                        <p class="menu-label">
                            Menu
                        </p>
                        <ul class="menu-list">
                            @include("components.menu.entry",['link'=>'/admin/users','caption'=>trans('menu.users')])
                            @include("components.menu.entry",['link'=>'/admin/stocks','caption'=>trans('menu.stocks')])
                            @include("components.menu.entry",['link'=>'/admin/accounts','caption'=>trans('menu.accounts')])
                            @include("components.menu.entry",['link'=>'/admin/transactions','caption'=>trans('menu.transactions')])
                        </ul>
                        <p class="menu-label">
                            Something else
                        </p>
                    </aside>
                </div>
                <div class="column hero is-fullheight">
                    @section('body')
                    @show
                </div>
            </div>
            </section>
        </div>
        @section("scripts")
            <script type="application/javascript" src="{{asset('js/app.js')}}"></script>
        @show
    </body>
</html>