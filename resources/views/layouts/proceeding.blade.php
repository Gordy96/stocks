@extends('layouts.auth')
@section('body')
    @if($state == 'mail_confirmation')
        <section class="hero is-light is-fullheight">
            <div class="hero-body">
                <div class="container">
                    <div class="columns is-centered">
                        <article class="card is-rounded">
                            <div class="card-content">
                                <form method="post">
                                    {{csrf_field()}}
                                    <div class="field">
                                        <p class="control has-icons-left has-icons-right">
                                            <input name="code" class="input" type="text" placeholder="Email code">
                                            <span class="icon is-small is-left">
                                                <i class="fa fa-envelope"></i>
                                            </span>
                                        </p>
                                    </div>
                                    <div class="field">
                                        <p class="control">
                                            <input type="submit" class="button is-primary" value="Submit">
                                        </p>
                                    </div>
                                </form>
                            </div>
                        </article>
                    </div>
                </div>
            </div>
        </section>
    @elseif($state == 'auth_success')
        Auth succeed
    @elseif($state == 'refresh' || $state == 'captcha')
        <script>
            setTimeout(function(){
                window.location.reload();
            }, 1000);
        </script>
    @elseif($state === 'fatal')
        <section class="hero is-light is-fullheight">
            <div class="hero-body">
                <div class="container">
                    <div class="columns is-centered">
                        <article class="card is-rounded">
                            <div class="card-content">
                                @foreach($errors as $error)
                                <article class="message is-danger">
                                    <div class="message-body">
                                         @lang('errors.'.$error)
                                    </div>
                                </article>
                                @endforeach
                            </div>
                        </article>
                    </div>
                </div>
            </div>
        </section>
    @endif
@endsection